// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

export default {
  loader: [{
    key: 'loader',
    src: 'loader.png',
    type: 'spritesheet',
    frameWidth: 128,
    frameHeight: 128,
  }],

  main: [{
    key: 'spizzy',
    type: 'atlasXML',
    textureURL: 'spizzy.png',
    atlasURL: 'spizzy.xml',
  }, {
    key: 'back',
    type: 'image',
  }, {
    key: 'wall',
    type: 'atlasXML',
    textureURL: 'walls.png',
    atlasURL: 'walls.xml',
  }, {
    key: 'MicroFont',
    type: 'bitmapFont',
    atlasURL: 'font/MicroFont.xml',
    textureURL: 'font/MicroFont.png',
  }, {
    key: 'MicroFontLight',
    type: 'bitmapFont',
    atlasURL: 'font/MicroFont.xml',
    textureURL: 'font/MicroFontLight.png',
  }, {
    key: 'music_dream1',
    type: 'audio',
    urls: ['audio/dream1.ogg'],
  }, {
    key: 'sound_tap',
    type: 'audio',
    urls: ['audio/tap.ogg'],
  }, {
    key: 'music_dream2',
    type: 'audio',
    urls: ['audio/dream2.ogg'],
  }, {
    key: 'music_dream3',
    type: 'audio',
    urls: ['audio/dream3.ogg'],
  }, {
    key: 'music_dream4',
    type: 'audio',
    urls: ['audio/dream4.ogg'],
  }, {
    key: 'music_dream5',
    type: 'audio',
    urls: ['audio/dream5.ogg'],
  }, {
    key: 'sound_bap',
    type: 'audio',
    urls: ['audio/bap.ogg'],
  }, {
    key: 'sound_dud',
    type: 'audio',
    urls: ['audio/dud.ogg'],
  }, {
    key: 'sound_ow',
    type: 'audio',
    urls: ['audio/ow.ogg'],
  }, {
    key: 'sound_bump',
    type: 'audio',
    urls: ['audio/bump.ogg'],
  }],
};
