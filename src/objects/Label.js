// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

export default function Label(game, x = 0, y = 0, text = '', font = 'MicroFont', size = 5) {
  const label = game.add.bitmapText(x, y, font, text, size);

  label.set = function set(value) {
    label.text = value;
  };

  label.get = function get() {
    return label.text;
  };

  label.add = function add(value) {
    const sum = parseInt(label.text, 10) + parseInt(value, 10);
    label.text = sum.toString();
  };

  label.place = function place(a, b) {
    label.x = a;
    label.y = b;
  };

  return label;
}
