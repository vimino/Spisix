// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'; // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'; // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'; // eslint-disable-line

import Label from '../objects/Label';

if (window.totalTime === undefined) {
  window.totalTime = 0.0;
}
let gameLevel;
if (gameLevel === undefined) {
  gameLevel = 6; // first level
}

export default class Game extends Phaser.State {
  create() {
    // this.background = this.game.add.sprite(0, 0, 'back');

    function buildMap(self, map) {
      const edgeRight = map.game.width;
      const edgeBottom = map.game.height;

      if (gameLevel === 1) { // 11111111111111111111111111111111111111111111111
        let corner = map.create(0, 0, 'wall', 'crnr');

        corner = map.create(edgeRight, 0, 'wall', 'crnr');
        corner.angle = 90;
        corner.body.offset.setTo(-8, 0);

        corner = map.create(edgeRight, edgeBottom, 'wall', 'crnr');
        corner.angle = 180;
        corner.body.offset.setTo(-8, -8);

        corner = map.create(0, edgeBottom, 'wall', 'crnr');
        corner.angle = 270;
        corner.body.offset.setTo(0, -8);

        const horizontal = (map.width / 8) - 1;
        const vertical = (map.height / 8) - 1;
        let wall = null;

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            0, i * 8,
            'wall', (i > 1 && i < vertical - 1) ? 'hard' : 'wall0',
          );
        }

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(8 + (i * 8), 0, 'wall', 'wall0');
          wall.angle = 90;
          wall.body.offset.setTo(-8, 0);
        }

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            edgeRight, 8 + (i * 8),
            'wall', (i > 1 && i < vertical - 1) ? 'bump' : 'wall0',
          );
          wall.angle = 180;
          wall.body.offset.setTo(-8, -8);
        }

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(i * 8, edgeBottom, 'wall', 'wall0');
          wall.angle = 270;
          wall.body.offset.setTo(0, -8);
        }
      } else if (gameLevel === 2) { // 2222222222222222222222222222222222222222
        let corner = map.create(0, 0, 'wall', 'hard');

        corner = map.create(edgeRight, 0, 'wall', 'hard');
        corner.angle = 90;
        corner.body.offset.setTo(-8, 0);

        corner = map.create(edgeRight, edgeBottom, 'wall', 'hard');
        corner.angle = 180;
        corner.body.offset.setTo(-8, -8);

        corner = map.create(0, edgeBottom, 'wall', 'hard');
        corner.angle = 270;
        corner.body.offset.setTo(0, -8);

        const horizontal = (map.width / 8) - 1;
        const vertical = (map.height / 8) - 1;
        let wall = null;

        // Left Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            0, i * 8,
            'wall', (i < 4 || i > horizontal - 4) ? 'hard' : 'bump',
          );
        }

        // Top Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            8 + (i * 8), 0,
            'wall', (i < 4 || i > horizontal - 4) ? 'hard' : 'wall0',
          );
          wall.angle = 90;
          wall.body.offset.setTo(-8, 0);
        }

        // Rught Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            edgeRight, 8 + (i * 8),
            'wall', (i < 4 || i > horizontal - 4) ? 'hard' : 'bump',
          );
          wall.angle = 180;
          wall.body.offset.setTo(-8, -8);
        }

        // Bottom Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            i * 8, edgeBottom,
            'wall', (i < 4 || i > horizontal - 4) ? 'hard' : 'wall0',
          );
          wall.angle = 270;
          wall.body.offset.setTo(0, -8);
        }
      } else if (gameLevel === 3) { // 3333333333333333333333333333333333333333
        let corner = map.create(0, 0, 'wall', 'crnr');

        corner = map.create(edgeRight, 0, 'wall', 'crnr');
        corner.angle = 90;
        corner.body.offset.setTo(-8, 0);

        corner = map.create(edgeRight, edgeBottom, 'wall', 'crnr');
        corner.angle = 180;
        corner.body.offset.setTo(-8, -8);

        corner = map.create(0, edgeBottom, 'wall', 'crnr');
        corner.angle = 270;
        corner.body.offset.setTo(0, -8);

        const horizontal = (map.width / 8) - 1;
        const vertical = (map.height / 8) - 1;
        let wall = null;

        // Left Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            0, i * 8,
            'wall', (i < 5 || i > horizontal - 5) ? 'wall0' : 'bump',
          );
        }

        // Top Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            8 + (i * 8), 0,
            'wall', (i < 5 || i > horizontal - 5) ? 'wall0' : 'bump',
          );
          wall.angle = 90;
          wall.body.offset.setTo(-8, 0);
        }

        // Rught Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            edgeRight, 8 + (i * 8),
            'wall', (i < 5 || i > horizontal - 5) ? 'wall0' : 'bump',
          );
          wall.angle = 180;
          wall.body.offset.setTo(-8, -8);
        }

        // Bottom Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            i * 8, edgeBottom, 'wall',
            (i < 5 || i > horizontal - 5) ? 'wall0' : 'bump',
          );
          wall.angle = 270;
          wall.body.offset.setTo(0, -8);
        }
      } else if (gameLevel === 4) { // 4444444444444444444444444444444444444444
        let corner = map.create(0, 0, 'wall', 'hard');

        corner = map.create(edgeRight, 0, 'wall', 'bump');
        corner.angle = 90;
        corner.body.offset.setTo(-8, 0);

        corner = map.create(edgeRight, edgeBottom, 'wall', 'hard');
        corner.angle = 180;
        corner.body.offset.setTo(-8, -8);

        corner = map.create(0, edgeBottom, 'wall', 'crnr');
        corner.angle = 270;
        corner.body.offset.setTo(0, -8);

        const horizontal = (map.width / 8) - 1;
        const vertical = (map.height / 8) - 1;
        let wall = null;

        // Left Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            0, i * 8,
            'wall', (i < 9) ? 'hard' : 'wall0',
          );
        }

        // Top Wall

        for (let i = 1; i < horizontal; i += 1) {
          let type = 'bump';
          if (i < 8) {
            if (i > 4) {
              type = 'spik';
            } else {
              type = 'hard';
            }
          }

          wall = map.create(
            8 + (i * 8), 0,
            'wall', type,
          );
          wall.angle = 90;
          wall.body.offset.setTo(-8, 0);
        }

        // Rught Wall

        for (let i = 1; i < vertical; i += 1) {
          let type = 'hard';
          if (i < 11) {
            if (i > 7) {
              type = 'spik';
            } else {
              type = 'bump';
            }
          }

          wall = map.create(
            edgeRight, 8 + (i * 8),
            'wall', type,
          );
          wall.angle = 180;
          wall.body.offset.setTo(-8, -8);
        }

        // Bottom Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            i * 8, edgeBottom,
            'wall', (i > 5) ? 'hard' : 'wall0',
          );
          wall.angle = 270;
          wall.body.offset.setTo(0, -8);
        }
      } else if (gameLevel === 5) { // 5555555555555555555555555555555555555555
        let corner = map.create(0, 0, 'wall', 'hard');

        corner = map.create(edgeRight, 0, 'wall', 'hard');
        corner.angle = 90;
        corner.body.offset.setTo(-8, 0);

        corner = map.create(edgeRight, edgeBottom, 'wall', 'hard');
        corner.angle = 180;
        corner.body.offset.setTo(-8, -8);

        corner = map.create(0, edgeBottom, 'wall', 'hard');
        corner.angle = 270;
        corner.body.offset.setTo(0, -8);

        const horizontal = (map.width / 8) - 1;
        const vertical = (map.height / 8) - 1;
        let wall = null;

        // Left Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            0, i * 8,
            'wall', (i < 6 || i > horizontal - 6) ? 'hard' : 'spik',
          );
        }

        // Top Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            8 + (i * 8), 0,
            'wall', (i < 6 || i > horizontal - 6) ? 'wall0' : 'hard',
          );
          wall.angle = 90;
          wall.body.offset.setTo(-8, 0);
        }

        // Rught Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            edgeRight, 8 + (i * 8),
            'wall', (i < 6 || i > horizontal - 6) ? 'hard' : 'spik',
          );
          wall.angle = 180;
          wall.body.offset.setTo(-8, -8);
        }

        // Bottom Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            i * 8, edgeBottom,
            'wall', (i < 5 || i > horizontal - 5) ? 'hard' : 'bump',
          );
          wall.angle = 270;
          wall.body.offset.setTo(0, -8);
        }
      } else if (gameLevel === 6) { // 6666666666666666666666666666666666666666
        let corner = map.create(0, 0, 'wall', 'hard');

        corner = map.create(edgeRight, 0, 'wall', 'hard');
        corner.angle = 90;
        corner.body.offset.setTo(-8, 0);

        corner = map.create(edgeRight, edgeBottom, 'wall', 'hard');
        corner.angle = 180;
        corner.body.offset.setTo(-8, -8);

        corner = map.create(0, edgeBottom, 'wall', 'hard');
        corner.angle = 270;
        corner.body.offset.setTo(0, -8);

        const horizontal = (map.width / 8) - 1;
        const vertical = (map.height / 8) - 1;
        let wall = null;

        // Left Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            0, i * 8,
            'wall', (i < 5 || i > horizontal - 5) ? 'hard' : 'spik',
          );
        }

        // Top Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            8 + (i * 8), 0,
            'wall', (i < 4 || i > horizontal - 4) ? 'wall0' : 'hard',
          );
          wall.angle = 90;
          wall.body.offset.setTo(-8, 0);
        }

        // Rught Wall

        for (let i = 1; i < vertical; i += 1) {
          wall = map.create(
            edgeRight, 8 + (i * 8),
            'wall', (i < 5 || i > horizontal - 5) ? 'hard' : 'spik',
          );
          wall.angle = 180;
          wall.body.offset.setTo(-8, -8);
        }

        // Bottom Wall

        for (let i = 1; i < horizontal; i += 1) {
          wall = map.create(
            i * 8, edgeBottom,
            'wall', (i < 5 || i > horizontal - 5) ? 'hard' : 'wall0',
          );
          wall.angle = 270;
          wall.body.offset.setTo(0, -8);
        }
      }

      map.setAll('body.immovable', true);
      map.setAll('body.collideWorldBounds', true);
    }

    this.map = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
    // this.game.add.group();
    buildMap(this, this.map);

    this.thoughts = Label(this.game, 78, 48, '', 'MicroFontLight');
    this.thoughts.anchor.setTo(0.5, 0.5);
    this.thoughts.visible = false;

    this.subtho = Label(this.game, 86, 38, '', 'MicroFontLight');
    this.subtho.anchor.setTo(0.5, 0.5);
    this.subtho.scale.setTo(2);
    this.thoughts.visible = false;

    function RestartGame(next) {
      if (next && gameLevel > 6) {
        gameLevel = 1;
        this.state.start('Ending');
      } else {
        if (!next) {
          gameLevel = 1;
          window.totalTime = 0.0;
        }
        this.state.start(next ? 'Game' : 'Intro');
      }
    }

    function GameOver(next = false, context = undefined) {
      let self = this;
      if (context !== undefined) { self = context; }

      self.spizzy.body.stop();
      self.spizzy.visible = false;

      self.subtho.place(64, 60);
      self.subtho.set(next ? 'Dream Over' : 'Game Over');
      self.subtho.visible = true;

      let timeTaken = Phaser.Math.roundTo(
        (Date.now() - self.gameStart) / 1000.0,
        -2,
      );
      window.totalTime = Phaser.Math.roundTo(window.totalTime + timeTaken, -2);
      self.thoughts.frame = 'MicroFont';
      self.thoughts.place(64, 72);
      if (next) {
        timeTaken = Phaser.Math.roundTo(window.totalTime, -2);
        self.thoughts.set(timeTaken.toString());
      } else {
        self.thoughts.set(`Total: ${window.totalTime}`);
      }
      self.thoughts.visible = true;

      self.touchCount = null;
      gameLevel += 1;

      self.game.time.events.add(Phaser.Timer.SECOND * 5, RestartGame, self, next);
    }

    this.spizzy = this.game.add.sprite(64, 64, 'spizzy', 'sleep1');
    this.spizzy.anchor.setTo(0.5, 0.5);

    // Color everything

    this.spizzy.tint = window.mainColor;
    this.thoughts.tint = window.mainColor;
    this.subtho.tint = window.mainColor;

    this.stage.backgroundColor = `#${window.mapColor}`;
    // this.background.tint = parseInt(window.mapColor, 16);
    this.map.setAll('tint', parseInt(window.mapColor, 16));

    this.game.physics.arcade.enable(this.spizzy);
    this.spizzy.body.bounce.set(0.8);
    // this.spizzy.body.setCircle(16);

    this.spizzy.checkWorldBounds = true;
    this.spizzy.events.onOutOfBounds.add(GameOver, this);

    this.touched = false;
    this.touchCount = 0;

    function WakeUp() {
      this.thoughts.place(this.spizzy.x, this.spizzy.y - 28);
      if (gameLevel === 1) {
        this.thoughts.set('I\'m still dreaming?\n\nBut something changed...');
      } else if (gameLevel === 2) {
        this.thoughts.set('Dreaming again...\n\nWill this never stop?');
      } else if (gameLevel === 3) {
        this.thoughts.set('How long will this last?\n\nI\'m tired of dreaming...');
        this.spizzy.animations.play('dang');
      } else if (gameLevel === 4) {
        this.thoughts.set('I\'m sick of spinning...\n\nWhen will it end?');
        this.spizzy.animations.play('dang');
      } else if (gameLevel === 5) {
        this.thoughts.set('What a nightmare,\n\nI must break through!');
        this.spizzy.animations.play('dang');
      } else if (gameLevel === 6) {
        this.thoughts.set('I have to wake up,\n\nplease wake up!');
        this.spizzy.animations.play('dang');
      }
      this.thoughts.visible = true;
      this.touchCount = 1;
    }

    this.spizzy.animations.add('wakeup', Phaser.Animation.generateFrameNames('wakeup', 0, 9), 3, false, false)
      .onComplete.add(WakeUp, this);

    this.spizzy.animations.add('dang', Phaser.Animation.generateFrameNames('dang', 0, 1), 4, true, false);

    function GoIdle() {
      this.spizzy.animations.play('idle');
    }

    function GoBlink() {
      if (this.game.rnd.integerInRange(0, 9) < 2) {
        this.spizzy.animations.play('blink');
      }
    }

    function StayDizzy() {
      if (this.game.rnd.integerInRange(0, 9) < 4) {
        this.spizzy.animations.play('recover');
      }
    }

    this.spizzy.animations.add('idle', Phaser.Animation.generateFrameNames('idle', 0, 9), 1, true, false)
      .onLoop.add(GoBlink, this);

    this.spizzy.animations.add('blink', Phaser.Animation.generateFrameNames('blink', 0, 2), 6, false, false)
      .onComplete.add(GoIdle, this);

    this.spizzy.animations.add('dizzy', Phaser.Animation.generateFrameNames('dizzy', 0, 3), 12, true, false)
      .onLoop.add(StayDizzy, this);
    this.spizzy.animations.add('recover', Phaser.Animation.generateFrameNames('dang', 0, 1), 4, false, false)
      .onComplete.add(GoIdle, this);

    this.spizzy.animations.play('wakeup');

    this.charge = 0;
    function spin() {
      if (this.game.input.activePointer.isDown) {
        if (this.charge < 30) {
          this.charge += 10;
        } else if (this.charge < 270) {
          this.charge += 20;
        }
        this.spizzy.angle += this.charge;
        this.spizzy.animations.play('dizzy');
      } else {
        this.charge /= 2.0;
        if (this.charge < 10) {
          this.charge = 0;
        } else {
          this.spizzy.angle += this.charge;
        }
      }
    }

    this.spinTimer = this.game.time.create(false);
    this.spinTimer.loop(100, spin, this);
    this.spinTimer.start();
    this.spinTimer.pause();

    this.HandleCollision = function HandleCollision(source, target) {
      const wall = target;

      // Get the Normalized impact
      const force = Math.sqrt((source.body.velocity.x * source.body.velocity.x) +
        (source.body.velocity.y * source.body.velocity.y));

      const name = wall.frameName.substr(0, 4);
      const id = parseInt(wall.frameName.substr(4), 10) + 1;

      if (name === 'hard') {
        window.audio.play('fxDud');

        if (this.spizzy.body.velocity.x > 10) {
          this.spizzy.body.velocity.x /= 2.0;
          this.spizzy.body.velocity.y /= 2.0;
        }
      } else if (name === 'bump') {
        window.audio.play('fxBump');

        if (this.spizzy.body.velocity.x < 200) {
          this.spizzy.body.velocity.x *= 2.0;
        } else {
          this.spizzy.body.velocity.x = 200;
        }

        if (this.spizzy.body.velocity.y < 200) {
          this.spizzy.body.velocity.y *= 2.0;
        } else {
          this.spizzy.body.velocity.y = 200;
        }
      } else if (name === 'spik') {
        if (force < 50.0) { return; } // don't kill Spizzy if she's quite slow
        window.audio.play('fxOw');
        GameOver(false, this);
      } else if (name === 'wall') {
        if (force < 50.0) {
          window.audio.play('fxDud');
        } else {
          window.audio.play('fxBap');
          if (id < 4) { // damage the wall
            wall.frameName = name + id.toString();
          } else { // destroy the wall
            wall.kill();
          }
        }
      }
    };

    this.gameStart = Date.now();
    window.audio.stopAll();
    // 1,2 = Dream2, 3,4 = Dream3, 5 = Dream4
    const dream = 1 + Math.ceil(gameLevel / 2);
    window.audio.loop(`msDream${dream}`);
  }

  update() {
    if (this.game.input.activePointer.isDown) {
      if (this.touchCount === null) { return; } // Dream/Game Over
      this.touched = true;
    }

    if (this.touched && this.game.input.activePointer.isUp) {
      if (this.touchCount === null) { return; } // Dream/Game Over
      this.touched = false;

      if (this.charge > 0) { // Launch the Fox
        const deltaX = (this.game.input.activePointer.x - this.spizzy.x) * (this.charge / 30.0);
        const deltaY = (this.game.input.activePointer.y - this.spizzy.y) * (this.charge / 30.0);

        this.spizzy.body.velocity.setTo(deltaX, deltaY);
        this.charge = 0;
      }

      if (this.touchCount === 1) {
        this.touchCount += 1;
        this.thoughts.visible = false;
        this.spizzy.animations.play('idle');
        this.spinTimer.resume();
      }
    }

    this.game.physics.arcade.collide(this.spizzy, this.map, this.HandleCollision, null, this);
  }

  // render() {
  //   this.game.debug.body(this.spizzy);
  //
  //   this.map.forEach(function RenderBody(wall) {
  //     this.game.debug.body(wall);
  //   }, this);
  // }
}
