// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import assets from '../assets';

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'; // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'; // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'; // eslint-disable-line

export default class Boot extends Phaser.State {
  preload() {
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    // this.scale.fullScreenScaleMode = this.scale.scaleMode;

    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;

    Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
    this.game.renderer.renderSession.roundPixels = true;
    this.stage.smoothed = false;

    this.load.path = './assets/';
    this.load.pack('loader', null, assets);

    function RandomColor(high) {
      const chars = high ? 'bcdef' : '23456';
      let color = '';
      for (let i = 0; i < 6; i += 1) {
        color += chars[Math.floor(Math.random() * chars.length)];
      }
      return color;
    }

    window.mainColor = parseInt(RandomColor(true), 16);
    window.mapColor = RandomColor(false);
    this.stage.backgroundColor = `#${window.mapColor}`;
  }

  create() {
    this.state.start('Loader');
  }
}
