// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'; // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'; // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'; // eslint-disable-line

import Label from '../objects/Label';

export default class Intro extends Phaser.State {
  create() {
    function buildMap(self, map) {
      const edgeRight = map.game.width;
      const edgeBottom = map.game.height;

      let corner = map.create(0, 0, 'wall', 'crnr');

      corner = map.create(edgeRight, 0, 'wall', 'crnr');
      corner.angle = 90;
      corner.body.offset.setTo(-8, 0);

      corner = map.create(edgeRight, edgeBottom, 'wall', 'crnr');
      corner.angle = 180;
      corner.body.offset.setTo(-8, -8);

      corner = map.create(0, edgeBottom, 'wall', 'crnr');
      corner.angle = 270;
      corner.body.offset.setTo(0, -8);

      const horizontal = (map.width / 8) - 1;
      const vertical = (map.height / 8) - 1;
      let wall = null;

      for (let i = 1; i < vertical; i += 1) {
        wall = map.create(0, i * 8, 'wall', 'wall0');
      }

      for (let i = 1; i < horizontal; i += 1) {
        wall = map.create(8 + (i * 8), 0, 'wall', 'wall0');
        wall.angle = 90;
        wall.body.offset.setTo(-8, 0);
      }

      for (let i = 1; i < vertical; i += 1) {
        wall = map.create(edgeRight, 8 + (i * 8), 'wall', 'wall0');
        wall.angle = 180;
        wall.body.offset.setTo(-8, -8);
      }

      for (let i = 1; i < horizontal; i += 1) {
        wall = map.create(i * 8, edgeBottom, 'wall', 'wall0');
        wall.angle = 270;
        wall.body.offset.setTo(0, -8);
      }

      map.setAll('body.immovable', true);
      map.setAll('body.collideWorldBounds', true);
    }

    this.map = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
    // this.game.add.group();
    buildMap(this, this.map);

    this.thoughts = Label(this.game, 78, 48, 'Z');
    this.thoughts.anchor.setTo(0.5, 0.5);

    this.subtho = Label(this.game, 86, 38, 'Z', 'MicroFontLight');
    this.subtho.anchor.setTo(0.5, 0.5);
    this.subtho.scale.setTo(2);

    this.thouTween = this.add.tween(this.thoughts).to(
      { x: 86, y: 56 },
      Phaser.Timer.SECOND * 2,
      Phaser.Easing.Linear.None,
      true, // auto-start
      0, // delay
      -1, // repeat (-1 = infinite)
      true, // yoyo
    );

    this.subthoTween = this.add.tween(this.subtho).to(
      { x: 98, y: 54 },
      Phaser.Timer.SECOND * 2,
      Phaser.Easing.Linear.None,
      true, // auto-start
      0, // delay
      -1, // repeat (-1 = infinite)
      true, // yoyo
    );

    function GameOver() {
      this.spizzy.body.stop();
      this.spizzy.visible = false;

      this.thouTween.stop();
      this.subthoTween.stop();

      this.subtho.place(64, 60);
      this.subtho.set('Dream Over');
      this.subtho.visible = true;

      const timeTaken = (Date.now() - this.gameStart) / 1000.0;
      this.thoughts.place(64, 72);
      this.thoughts.set(timeTaken.toString());
      this.thoughts.visible = true;

      this.game.time.events.add(Phaser.Timer.SECOND * 5, function RestartGame() {
        this.state.start('Game');
      }, this);
    }

    this.spizzy = this.game.add.sprite(64, 64, 'spizzy', 'sleep1');
    this.spizzy.anchor.setTo(0.5, 0.5);

    // Color everything

    this.spizzy.tint = window.mainColor;
    this.thoughts.tint = window.mainColor;
    this.subtho.tint = window.mainColor;

    this.stage.backgroundColor = `#${window.mapColor}`;
    this.map.setAll('tint', parseInt(window.mapColor, 16));

    this.game.physics.arcade.enable(this.spizzy);
    this.spizzy.body.bounce.set(0.8);
    // this.spizzy.body.setCircle(16);

    this.spizzy.checkWorldBounds = true;
    this.spizzy.events.onOutOfBounds.add(GameOver, this);

    this.touched = false;
    this.touchCount = 0;

    function BackToSleep() {
      if (this.touchCount > -3) {
        this.spizzy.animations.play('sleep');
        this.touchCount = 0;
      } else {
        this.spizzy.animations.play('wakeup');
        this.thoughts.visible = false;
        this.subtho.visible = false;
        this.thouTween.stop();
        this.subthoTween.stop();
      }
    }

    function WakeUp() {
      this.thoughts.place(this.spizzy.x, this.spizzy.y - 26);
      this.thoughts.set('Where am I ?');
      this.thoughts.visible = true;
      this.touchCount = 1;
    }

    this.spizzy.animations.add('sleep', Phaser.Animation.generateFrameNames('sleep', 0, 3), 2, true, false);
    this.spizzy.animations.add('touch', Phaser.Animation.generateFrameNames('touch', 0, 2), 2, false, false)
      .onComplete.add(BackToSleep, this);

    this.spizzy.animations.add('wakeup', Phaser.Animation.generateFrameNames('wakeup', 0, 9), 3, false, false)
      .onComplete.add(WakeUp, this);

    this.spizzy.animations.add('dang', Phaser.Animation.generateFrameNames('dang', 0, 1), 4, true, false);

    function GoIdle() {
      this.spizzy.animations.play('idle');
    }

    function GoBlink() {
      if (this.game.rnd.integerInRange(0, 9) < 2) {
        this.spizzy.animations.play('blink');
      }
    }

    function StayDizzy() {
      if (this.game.rnd.integerInRange(0, 9) < 4) {
        this.spizzy.animations.play('recover');
      }
    }

    this.spizzy.animations.add('idle', Phaser.Animation.generateFrameNames('idle', 0, 9), 1, true, false)
      .onLoop.add(GoBlink, this);

    this.spizzy.animations.add('blink', Phaser.Animation.generateFrameNames('blink', 0, 2), 6, false, false)
      .onComplete.add(GoIdle, this);

    this.spizzy.animations.add('dizzy', Phaser.Animation.generateFrameNames('dizzy', 0, 3), 12, true, false)
      .onLoop.add(StayDizzy, this);
    this.spizzy.animations.add('recover', Phaser.Animation.generateFrameNames('dang', 0, 1), 4, false, false)
      .onComplete.add(GoIdle, this);

    this.spizzy.animations.play('sleep');

    this.charge = 0;
    function spin() {
      if (this.game.input.activePointer.isDown) {
        if (this.charge < 30) {
          this.charge += 10;
        } else if (this.charge < 360) {
          this.charge *= 2.0;
        }
        this.spizzy.angle += this.charge;
        this.spizzy.animations.play('dizzy');
      } else {
        this.charge /= 2.0;
        if (this.charge < 10) {
          this.charge = 0;
        } else {
          this.spizzy.angle += this.charge;
        }
      }
    }

    this.spinTimer = this.game.time.create(false);
    this.spinTimer.loop(100, spin, this);
    this.spinTimer.start();
    this.spinTimer.pause();

    this.HandleCollision = function HandleCollision(source, target) {
      const wall = target;

      // Get the Normalized impact
      const force = Math.sqrt((source.body.velocity.x * source.body.velocity.x) +
        (source.body.velocity.y * source.body.velocity.y));

      const name = wall.frameName.substr(0, 4);
      const id = parseInt(wall.frameName.substr(4), 10) + 1;

      // Can't break non-walls
      if (name === 'wall') {
        if (force < 50.0) {
          window.audio.play('fxDud');
        } else {
          window.audio.play('fxBap');

          if (id < 4) { // damage the wall
            wall.frameName = name + id.toString();
          } else { // destroy the wall
            wall.kill();
          }
        }
      }
    };

    this.gameStart = Date.now();
    window.audio.stopAll();
    window.audio.loop('msDream1');
  }

  update() {
    if (this.game.input.activePointer.isDown) {
      if (!this.touched && this.touchCount < 0) {
        window.audio.play('fxTap');
      }

      this.touched = true;

      if (this.touchCount < 0 && this.spizzy.animations.currentAnim.name === 'sleep') {
        this.spizzy.animations.stop();
        this.spizzy.animations.play('touch');
      }
    }

    if (this.touched && this.game.input.activePointer.isUp) {
      this.touched = false;

      if (this.charge > 0) { // Launch the Fox
        const deltaX = (this.game.input.activePointer.x - this.spizzy.x) * (this.charge / 30.0);
        const deltaY = (this.game.input.activePointer.y - this.spizzy.y) * (this.charge / 30.0);

        this.spizzy.body.velocity.setTo(deltaX, deltaY);
        this.charge = 0;
      }

      if (this.touchCount <= 0) {
        this.touchCount -= 1;
      } else if (this.touchCount < 5) {
        this.touchCount += 1;
        if (this.touchCount === 2) {
          this.spizzy.animations.play('dang');
          this.thoughts.place(this.spizzy.x, this.spizzy.y - 26);
          this.thoughts.set('I\'m stuck! This is bad.');
        } else if (this.touchCount === 3) {
          this.spizzy.animations.play('idle');
          this.thoughts.visible = false;
        } else if (this.touchCount === 4) { // Start the Spin timer
          this.spinTimer.resume();
        }
      }
    }

    this.game.physics.arcade.collide(this.spizzy, this.map, this.HandleCollision, null, this);
  }

  // render() {
  //   this.game.debug.body(this.spizzy);
  //
  //   this.map.forEach(function RenderBody(wall) {
  //     this.game.debug.body(wall);
  //   }, this);
  // }
}
