// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import assets from '../assets';

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'; // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'; // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'; // eslint-disable-line

export default class Loader extends Phaser.State {
  create() {
    this.load.path = './assets/';

    this.back = this.game.add.sprite(0, 0, 'loader', 0);
    this.front = this.game.add.sprite(0, 0, 'loader', 1);
    this.load.setPreloadSprite(this.front);

    // Color everything

    this.back.tint = window.mainColor;
    this.front.tint = window.mainColor;

    this.load.onLoadComplete.add(this.start, this);
    this.load.pack('main', null, assets);
    this.load.start();
  }

  start() {
    const globalGame = this.game;

    const audioFiles = {
      msDream1: 'music_dream1',
      msDream2: 'music_dream2',
      msDream3: 'music_dream3',
      msDream4: 'music_dream4',
      msDream5: 'music_dream5',
      fxTap: 'sound_tap',
      fxBap: 'sound_bap',
      fxDud: 'sound_dud',
      fxOw: 'sound_ow',
      fxBump: 'sound_bump',
    };
    window.audio = {};

    Object.keys(audioFiles).forEach((key) => {
      if (typeof (audioFiles[key]) === 'string') {
        window.audio[key] = globalGame.add.audio(audioFiles[key]);
      } else {
        window.audio[key] = [];
        audioFiles[key].forEach((name) => {
          window.audio[key].push(globalGame.add.audio(name));
        });
      }
    });

    // Decrease the music volume
    Object.keys(window.audio).forEach((name) => {
      if (name.substring(0, 2) === 'ms') {
        window.audio[name].volume = 0.2;
      }
    });

    window.audio.loop = function loop(name) {
      window.audio[name].loopFull();
    };

    window.audio.play = function play(name) {
      if (typeof (window.audio[name]) === 'object') {
        if (window.audio[name] instanceof Array) {
          globalGame.rnd.pick(window.audio[name]).play();
        } else {
          window.audio[name].play();
        }
      }
    };

    window.audio.stop = function stop(name) {
      if (typeof (window.audio[name]) !== 'object') { return; }
      if (window.audio[name] instanceof Array) {
        Object.keys(window.audio[name]).forEach((id) => {
          window.audio[name][id].stop();
        });
      } else {
        window.audio[name].stop();
      }
    };

    window.audio.stopAll = function stopAll() {
      Object.keys(window.audio).forEach((key) => {
        if (typeof (window.audio[key]) === 'object') {
          window.audio.stop(key);
        }
      });
    };

    window.audio.stopAll();

    this.back.visible = false;
    this.front.anchor.setTo(0.5, 0.5);
    this.front.position.setTo(64, 64);
    this.add.tween(this.front).to(
      { width: 32, height: 32 },
      Phaser.Timer.SECOND,
      Phaser.Easing.Cubic.In,
      true,
    ).onComplete.add(function StartGame() {
      this.state.start('Intro');
    }, this);
  }
}
