// Spisix, a game where you help Spizzy escape her strange Dreams
// Copyright (c) 2018, Vítor T. "vimino" Martins
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'; // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'; // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'; // eslint-disable-line

import Label from '../objects/Label';

export default class Ending extends Phaser.State {
  create() {
    this.thoughts = Label(this.game, 78, 48, '');
    this.thoughts.anchor.setTo(0.5, 0.5);

    this.subtho = Label(this.game, 64, 34, 'Game Over', 'MicroFontLight');
    this.subtho.anchor.setTo(0.5, 0.5);
    this.subtho.scale.setTo(2);
    this.subtho.visible = false;

    this.spizzy = this.game.add.sprite(64, 64, 'spizzy', 'sleep1');
    this.spizzy.anchor.setTo(0.5, 0.5);

    // Color everything

    this.spizzy.tint = window.mainColor;
    this.thoughts.tint = window.mainColor;
    this.subtho.tint = window.mainColor;

    this.stage.backgroundColor = `#${window.mapColor}`;

    this.game.physics.arcade.enable(this.spizzy);

    function Outside() {
      this.spizzy.body.stop();
    }

    this.spizzy.checkWorldBounds = true;
    this.spizzy.events.onOutOfBounds.add(Outside, this);

    function WakeUp() {
      this.thoughts.place(this.spizzy.x, this.spizzy.y - 26);
      this.thoughts.set('I\'m finally free!');
      this.thoughts.visible = true;
      this.spizzy.animations.play('happy');
      this.canTouch = true;
    }

    this.spizzy.animations.add(
      'wakeup',
      [
        'wakeup0', 'wakeup1', 'wakeup2', 'wakeup3',
        'wakeup4', 'wakeup5', 'wakeup6', 'wakeup7',
        'happy0', 'happy1', 'happy2', 'happy3',
      ],
      3,
      false,
      false,
    )
      .onComplete.add(WakeUp, this);

    this.spizzy.animations.add(
      'happy',
      ['happy3', 'happy4'],
      3,
      true,
      false,
    );

    this.spizzy.animations.play('wakeup');
    this.canRestart = false;

    window.audio.stopAll();
    window.audio.loop('msDream5');

    this.touched = false;
  }

  update() {
    if (!this.touched && this.canTouch && this.game.input.activePointer.isDown) {
      this.touched = true;

      if (this.canRestart) {
        this.state.start('Intro');
      }

      const timeTaken = Phaser.Math.roundTo(window.totalTime, -2);
      this.thoughts.set(`Thanks for Playing\n\nTotal: ${timeTaken} seconds`);
      this.thoughts.place(64, 94);
      this.subtho.visible = true;

      this.canRestart = true;
    }

    if (this.touched && this.game.input.activePointer.isUp) {
      this.touched = false;
    }
  }

  // render() {
  //   this.game.debug.body(this.spizzy);
  //
  //   this.map.forEach(function RenderBody(wall) {
  //     this.game.debug.body(wall);
  //   }, this);
  // }
}
