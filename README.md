# Spisix

I had an idea for a game where a character was emprisioned but could spin and hit the walls to escape.<br/>
When I learned about the upcoming [Mini Jam](https://itch.io/jam/mini-jam) I felt compeled to try it out.

For the name, I mixed Spiffy Physics. Later renaming Spiffy to Spizzy (the main character).

The rules are simple: Make a game with **4 colors** (can't change) in **128x128** pixels.<br/>
The theme was **Limitations** and **Breakout**, which goes hand-in-hand with simple pixelart.<br/>
In this case, *Spizzy is inside a ball and needs to break out of her Dreams/Nightmares*.

# [Play the game on Itch.io](https://vimino.itch.io/spisix)

### Commands
This is a npm package and supports the following commands:
- **npm run lint** : Checks the source files (in the 'src' folder) for errors
- **npm run build** : Builds the game.js file and places it in the *public* folder
- **npm test** : Runs webpack-dev-server which serves the *public* folder in **localhost:8080**

### License

Copyright &copy; 2018, Vítor "VIMinO" Martins

All Rights are Reserved for the assets.js file, as well as XML and JSON files,<br/>
Remaining source code files (html,js) are licensed under the [GPLv3 License ](https://www.gnu.org/licenses/gpl-3.0.en.html)<br/>
Media (png,ogg) files are licensed under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)

[![gplv3 button](https://www.gnu.org/graphics/gplv3-88x31.png)](https://www.gnu.org/licenses/gpl.html)
[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)

