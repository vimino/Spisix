var path = require('path')
var webpack = require('webpack')

const SRC_DIR = path.resolve(__dirname, 'src')
const BUILD_DIR = path.resolve(__dirname, 'public')

module.exports = {
  entry: path.join(SRC_DIR, '/app.js'),

  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],

  output: {
    filename: 'game.js',
    path: BUILD_DIR
  },

  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        include: SRC_DIR,
        loader: 'eslint-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        include: SRC_DIR,
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  stats: {
    colors: true
  },
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    hot: true, // use hot-module-replacement
    contentBase: BUILD_DIR,
    port: 8080,
    stats: 'minimal',
  }
}
